export default class Poney {
  public id: string;
  public userId: string;
  public title : string;
  public body: string;

  constructor(id : string , userId: string,title : string,body : string){
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.body = body;
  }
}
