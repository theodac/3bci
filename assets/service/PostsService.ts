export default class PostsService {

  public axios: any;

  constructor($axios: any){
    this.axios = $axios;
  }

  findAll(){
    return this.axios({
      method: 'get',
      url : 'https://jsonplaceholder.typicode.com/posts'
    }).then( (response: any) => {
      console.log(response)
      return response.data
    })
  }

  create(title : string , body : string ){
    return this.axios({
      method: 'post',
      url : 'https://jsonplaceholder.typicode.com/posts',
      data : {
        title : title,
        body : body,
        userId: '1'
      }
    }).then((response:any) => {
      return response.data;
    })
  }
}
