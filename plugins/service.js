import PostsService from '~/assets/service/PostsService'

export default (ctx,inject) => {
  const services = {
    posts: new PostsService(ctx.$axios),


  }

  inject('services',services);
}
